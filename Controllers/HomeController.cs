﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using _LAB1_PATH_.Models;

namespace _LAB1_PATH_.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Pack()
        {

            return View();
        }
        public IActionResult Keeler()
        {

            return View();
        }
        public IActionResult Moore()
        {


            return View();
        }

        public IActionResult Simpson()
        {

            return View();
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
